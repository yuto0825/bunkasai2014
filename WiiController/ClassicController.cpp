#include "ClassicController.h"


ClassicController::ClassicController()
{
	vals[0] = &a;
	vals[1] = &b;
	vals[2] = &x;
	vals[3] = &y;
	vals[4] = &start;
	vals[5] = &select;
	vals[6] = &home;
	vals[7] = &r;
	vals[8] = &l;
	vals[9] = &zr;
	vals[10] = &zl;
	vals[11] = &up;
	vals[12] = &down;
	vals[13] = &left;
	vals[14] = &right;
	vals[15] = &rx;
	vals[16] = &ry;
	vals[17] = &lx;
	vals[18] = &ly;
	vals[19] = &blt;
	vals[20] = &brt;
}
ClassicController::~ClassicController()
{

}

ClassicController ClassicController::latest;

const char* ClassicController::names [] = {
	"aBtn ",
	"bBtn ",
	"xBtn ",
	"yBtn ",
	"+Btn ",
	"-Btn ",
	"Home ",
	"r Tr ",
	"l Tr ",
	"zrTr ",
	"zlTr ",
	"up   ",
	"down ",
	"left ",
	"right",
	"r_x  ",
	"r_y  ",
	"l_x  ",
	"l_y  ",
	"blt  ",
	"brt  "
};
//右xスティック初期値
signed char ClassicController::rxSt;
//右yスティック初期値
signed char ClassicController::rySt;
//左xスティック初期値
signed char ClassicController::lxSt;
//左yスティック初期値
signed char ClassicController::lySt;

