#ifdef wire_h
#else
#include <Wire.h>
#endif
#include "ClassicController.h"
/*
---デジタルピン---
2:砲台右 pedalR Btc1
5:砲台左 pedalL Btc2
7:発射 launch Btc3

---アナログ---
1:右モーターに関するコントローラー
2:左モーターに関するコントローラー
3:砲台上下のサーボに関するコントローラー

*/

//ボードの種類
#define uno 0

//ボタンのピン番号

//砲台右
const int pedalR = 2;
//砲台左
const int pedalL = 3;
//発射
const int launch = 20;

//前進右
const int fr = 4;
//後進右
const int br = 5;
//前進左
const int fl = 7;
//後進左
const int bl = 6;
//赤外線モジュールの最初のピン番号
const int cantsee = 8;
#if !uno
	const int cantsee2 = 19;
#endif
//アナログのピン番号
//const int RAn = 1;
//const int LAn = 2;
const int HAn = 3;

//i2cデータ用
const int i2cData = 4;
//i2cクロック用
const int i2cClock = 5;

//送信モード選択ピン LOW = mcr
#if uno
const int modeSelector = 14;
#else
const int modeSelector = 18;
#endif
//コントローラ選択用(LOW = wii)
const bool debugSelector = true;

////LOWならDebugFuncを実行する
//const int writeMode = 15;

//出力をバイナリにするか可読なフォーマットにするか
#define debug false

#if false
//送信毎のインターバル(ミリ秒)
#define interval (50)
//最終送信からのインターバル
#define interval2 (50)
#else
//送信毎のインターバル(ミリ秒)
#define interval (0)
//最終送信からのインターバル
#define interval2 (20)
//出力
#endif

//出力
#define wireDebug false

//UARTの速度
#define BaudRate (115200)

//デバッグ用関数(wii)
void debugFunc(void);

//コントロール元が(ボタン等→True、wii→False)
bool mode = true;

//キーボードが下がっているか
bool keyState[6] = { false, false, false, false, false, false };

void setup()
{

	//pinMode(pedalR, INPUT);
	//pinMode(pedalL, INPUT);
	//pinMode(launch, INPUT);
	pinMode(fr, INPUT_PULLUP);
	pinMode(br, INPUT_PULLUP);
	pinMode(fl, INPUT_PULLUP);
	pinMode(bl, INPUT_PULLUP);
	pinMode(pedalR, INPUT_PULLUP);
	pinMode(pedalL	, INPUT_PULLUP);
	pinMode(launch	, INPUT_PULLUP);
	pinMode(modeSelector, INPUT_PULLUP);
	//pinMode(debugSelector, INPUT_PULLUP);
#if !uno
	for (int i = 0; i < 5; i++)
	{
		pinMode(cantsee + i, INPUT);
	}
	pinMode(cantsee2, INPUT);
#endif
	//pinMode(writeMode, INPUT_PULLUP);
	pinMode(13, OUTPUT);

	Serial.begin(BaudRate);
#if !uno
	Serial1.begin(BaudRate);
	Keyboard.begin();
#endif
	if (debugSelector)
	{
		mode = true;
	}
	else
	{
		Wire.begin();
		ClassicController::init();
		ClassicController::receiveData();
		for (int i = 0; i < 100; i++)
		{
			delay(10);
			ClassicController::receiveData();
		}
		ClassicController::receiveData();
		ClassicController::receiveInit();
		ClassicController::receiveData();
		mode = false;
		//if (!digitalRead(writeMode))
		//{
			//debugFunc();
		//}
	}
	//while (true)
	//{
	//	for (int i = 0; i < 5; i++)
	//	{
	//		if (digitalRead(cantsee + i))
	//		{
	//			Serial.print('1');
	//		}
	//		else
	//		{
	//			Serial.print('0');
	//		}
	//	}
	//	if (digitalRead(cantsee2))
	//	{
	//		Serial.print('1');
	//	}
	//	else
	//	{
	//		Serial.print("asdfghjkl");
	//	}
	//	Serial.print("\r\n");
	//}
}


//クラシックコントローラの動作確認
void debugFunc(void){
	int i;
	int count;
	Serial.print(Sequence::clear());
	while (true)
	{		
		Serial.print(Sequence::xy(1, 1));
		Serial.print("state = ");
		Serial.print(ClassicController::receiveData());
		Serial.print("  ");
		for (i = 0; i < 4; i++)
		{
			Serial.print(ClassicController::names[i]);
			Serial.print(" = ");
			count = Serial.print((int) *ClassicController::latest.vals[i]);
			while (count < 4){
				Serial.print(" ");
				count++;
			}
		}
		Serial.print("\r\n");
		for (; i < 9; i++)
		{
			Serial.print(ClassicController::names[i]);
			Serial.print(" = ");
			count = Serial.print((int) *ClassicController::latest.vals[i]);
			while (count < 4){
				Serial.print(" ");
				count++;
			}
		}
		Serial.print("\r\n");
		for (; i < 14; i++)
		{
			Serial.print(ClassicController::names[i]);
			Serial.print(" = ");
			count = Serial.print((int) *ClassicController::latest.vals[i]);
			while (count < 4){
				Serial.print(" ");
				count++;
			}
		}
		Serial.print("\r\n");
		for (; i <19; i++)
		{
			Serial.print(ClassicController::names[i]);
			Serial.print(" = ");
			count = Serial.print((int) *ClassicController::latest.vals[i]);
			while (count < 4){
				Serial.print(" ");
				count++;
			}
		}
		Serial.print("\r\n");
		for (; i < 21; i++)
		{
			Serial.print(ClassicController::names[i]);
			Serial.print(" = ");
			count = Serial.print((int) *ClassicController::latest.vals[i]);
			while (count < 4){
				Serial.print(" ");
				count++;
			}
		}
		Serial.print("Flx = ");
		count = Serial.print((int) ClassicController::latest.formedLX());
		while (count < 4){
			Serial.print(" ");
			count++;
		}
		Serial.print("Fly = ");
		count = Serial.print((int) ClassicController::latest.formedLY());
		while (count < 4){
			Serial.print(" ");
			count++;
		}
		Serial.print("Frx = ");
		count = Serial.print((int) ClassicController::latest.formedRX());
		while (count < 4){
			Serial.print(" ");
			count++;
		}
		Serial.print("Fry = ");
		count = Serial.print((int) ClassicController::latest.formedRY());
		while (count < 4){
			Serial.print(" ");
			count++;
		}

	}
}

//1になっているビット数を数える
inline unsigned char bitCount(unsigned char bits){
	unsigned char res = 0;
	for (unsigned char i = 0; i < 8; i++)
	{
		if ((bits >> i) & 1)
		{
			res++;
		}
	}
	return res;
}


/*
0: 0--- ---+  (------ = 右モータ　　 = 6Bit = 64)(+ = パリティ = 1の数 & 1)
1: 0--- ---+  (------ = 左モータ　　 = 6Bit = 64)(+ = パリティ = 1の数 & 1)
2: 0--- ---+  (------ = サーボモータ = 6Bit = 64)(+ = パリティ = 1の数 & 1)
3: 1*** ---+  (*** 1,2,3バイト目のパリティ)(--- 砲台右、砲台左、発射)
*/
			
unsigned char buff;
unsigned char parity;

struct datas
{
	//0〜63
	unsigned char Rmotor;
	//0〜63
	unsigned char Lmotor;
	//0〜63
	unsigned char Servo;
	//01
	bool Rturn;
	//01
	bool Lturn;
	//01
	bool Burn;
};

datas data;
//
//struct ClassicController
//{
//	unsigned char RX : 6;
//	unsigned char RY : 6;
//	unsigned char LX : 5;
//	unsigned char LY : 5;
//
//};


void loop()
{


	//データ読み取り
	if (mode)
	{
		//ピンから状態を読み取り、dataに入れる

		if (digitalRead(modeSelector))
		{
			//戦車用

			//data.Rmotor = analogRead(RAn) >> 4;
			//data.Lmotor = analogRead(LAn) >> 4;

			if (digitalRead(fr))
			{
				if (digitalRead(br))
				{
					data.Rmotor = 32;
				}
				else
				{
					data.Rmotor = 63;
				}
			}
			else
			{
				if (digitalRead(br))
				{
					data.Rmotor = 0;
				}
				else
				{
					data.Rmotor = 32;
				}
			}
			if (digitalRead(fl))
			{
				if (digitalRead(bl))
				{
					data.Lmotor = 32;
				}
				else
				{
					data.Lmotor = 63;
				}
			}
			else
			{
				if (digitalRead(bl))
				{
					data.Lmotor = 0;
				}
				else
				{
					data.Lmotor = 32;
				}
			}

			data.Servo = analogRead(HAn) >> 4;
			data.Rturn = !digitalRead(pedalR);
			data.Lturn = !digitalRead(pedalL);
			data.Burn = !digitalRead(launch);
		}
		else{
			//ラジコンカー用
			data.Burn = false;
			//右がアクセル、左がブレーキ
			if (!digitalRead(pedalL))
			{
				//ブレーキが踏まれているなら
				data.Rmotor = 31;
			}
			else
			{
				if (!digitalRead(pedalR))
				{
					//アクセルのみ踏まれている
					if (!digitalRead(br))
					{
						//前進
						data.Rmotor = 63;
					}
					else
					{
						//後進
						data.Rmotor = 0;
					}
				}
				else{
					//フリー
					//スピードをフリーにする
					data.Burn = true;
				}
			}
			data.Servo = analogRead(HAn) >> 4;
		}
	}
	else
	{
		//クラシックコントローラからdataに読み取る
		while (!ClassicController::receiveData()){
			digitalWrite(13, LOW);
		}
		digitalWrite(13, HIGH);
		ClassicController &latest = ClassicController::latest;
		if (digitalRead(modeSelector))
		{
			//戦車用に読み取る
			if (latest.zr)
			{
				//後なし
				if (latest.r)
				{
					//前のみ
					data.Rmotor = 0;
				}
				else
				{
					//なし
					data.Rmotor = 32;
				}
			}
			else
			{
				if (latest.r)
				{
					//両方
					data.Rmotor = 32;
				}
				else
				{
					//後のみ
					data.Rmotor = 63;
				}
			}
			if (latest.zl)
			{
				//後なし
				if (latest.l)
				{
					//前のみ
					data.Lmotor = 0;
				}
				else
				{
					//なし
					data.Lmotor = 32;
				}
			}
			else
			{
				if (latest.l)
				{
					//両方
					data.Lmotor = 32;
				}
				else
				{
					//後のみ
					data.Lmotor = 63;
				}
			}

			data.Servo = latest.lx;
			data.Rturn = latest.right;
			data.Lturn = latest.left;
			data.Burn = latest.a;

		}
		else
		{
			//ラジコン用に読み取る
			unsigned char c1 = (latest.formedLY());
			unsigned char c2 = (latest.formedRX())/*(latest.formedLX())*/;
			//unsigned char c1 = (latest.ry);
			//unsigned char c2 = (latest.lx);
			data.Burn = false;
			data.Rmotor = c1;
			data.Servo = c2 << 1;
		}



	}


	//1Byte目
	//1023 >> 4 = 63
	buff = (data.Rmotor) << 1;
	buff |= (bitCount(buff) & 1);
	parity = (buff & 1) << 6;
#if debug
	Serial.print("\rB1 = ");
	for (int i = 7; i > 0; i--)
	{
		if (1 & (buff >> i))break;
		else Serial.write('0');
	}
	Serial.print(buff, BIN);
#else
#if uno
	Serial.write(buff);
#else
	Serial1.write(buff);
#endif
#endif // debug

#if interval
	delay(interval);
#endif


	//2Byte目
	//1023 >> 4 = 63
	buff = (data.Lmotor) << 1;
	buff |= (bitCount(buff) & 1);
	parity |= (buff & 1) << 5;
#if debug
	Serial.print(" , B2 = ");
	for (int i = 7; i > 0; i--)
	{
		if (1 & (buff >> i))break;
		else Serial.write('0');
	}
	Serial.print(buff, BIN);
#else
#if uno
	Serial.write(buff);
#else
	Serial1.write(buff);
#endif
#endif // debug

#if interval
	delay(interval);
#endif

	//3Byte目
	//1023 >> 4 = 63
	buff = (data.Servo) << 1;
	buff |= (bitCount(buff) & 1);
	parity |= (buff & 1) << 4;
#if debug
	Serial.print(" , B3 = ");
	for (int i = 7; i > 0; i--)
	{
		if (1 & (buff >> i))break;
		else Serial.write('0');
	}
	Serial.print(buff, BIN);
#else
#if uno
	Serial.write(buff);
#else
	Serial1.write(buff);
#endif
#endif // debug
#if interval
	delay(interval);
#endif

	//4Byte目
	buff = 0x80 | (data.Burn << 1) | (data.Lturn << 2) | (data.Rturn << 3) | parity;
	buff |= (bitCount(buff) & 1);
#if debug
	Serial.print(" , B4 = ");
	for (int i = 7; i > 0; i--)
	{
		if (1 & (buff >> i))break;
		else Serial.write('0');
	}
	Serial.print(buff, BIN);
#else
#if uno
	Serial.write(buff);
#else
	Serial1.write(buff);
#endif
#endif // debug

#if interval2
	delay(interval2);
#endif
#if !uno
	//キーボード
	for (int i = 0; i < 5; i++)
	{
		bool val = digitalRead(cantsee + i);
		if (val)
		{
			//反応なし
			if (keyState[i])
			{
				Keyboard.release('a' + i);
				keyState[i] = false;
			}
		}
		else
		{
			//反応あり
			if (!keyState[i])
			{
				Keyboard.write('a' + i);
				keyState[i] = true;
			}
		}
	}
	bool val = digitalRead(cantsee2);
	if (val)
	{
		//反応なし
		if (keyState[5])
		{
			Keyboard.release('f');
			keyState[5] = false;
		}
	}
	else
	{
		//反応あり
		if (!keyState[5])
		{
			Keyboard.write('f');
			keyState[5] = true;
		}
	}
#endif
}


