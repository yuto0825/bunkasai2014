#define wire_h
#include "Sequence.h"
#include <Wire.h>

//http://wiibrew.org/wiki/Wiimote/Extension_Controllers/Classic_Controller_Pro

const unsigned char i2cAdd(0x52);


inline unsigned char decodeByte(unsigned char data);


//クラシックコントローラのクラス
class ClassicController
{
public:

	signed char* vals[21];/* = { &a, &b, &x, &y, &start, &select, &home, &r, &l, &zr, &zl, &up, &down, &left, &right, &rx, &ry, &lx, &ly, &blt, &brt }*/

	static const char* names [];

	ClassicController();
	~ClassicController();

	//初期化する
	static unsigned char init(void){
		Wire.begin();					// join i2c bus as master 
		Wire.beginTransmission(i2cAdd);	// transmit 
		Wire.write((uint8_t) 0x40);		// sends memory address 
		Wire.write((uint8_t) 0x00);		// sends sent a zero.
		return Wire.endTransmission();	// stop transmitting 
	}

	static void receiveInit( void){
		rxSt = latest.rx;
		rySt = latest.ry;
		lxSt = latest.lx;
		lySt = latest.ly;
	}


	//データを受け取る
	static unsigned char receiveData(void){
		int cnt = 0;
		Wire.requestFrom(i2cAdd, (uint8_t)6);// request data from nunchuck
		while (Wire.available()) {
			// receive byte as an integer
			latest.rawData[cnt] = decodeByte(Wire.read());
			cnt++;
		}
		send0();  // send request for next data payload
		// If we recieved the 6 bytes, then go print them
		if (cnt >= 5) {
			latest.createData();
			return 1;   // success
		}
		return 0; //failure
	}

	//データ
	static ClassicController latest;

	//aボタン
	signed char a;
	//bボタン
	signed char b;
	//xボタン
	signed char x;
	//yボタン
	signed char y;
	//+ボタン
	signed char start;
	//-ボタン
	signed char select;
	//homeボタン
	signed char home;
	//rトリガー
	signed char r;
	//lトリガー
	signed char l;
	//zrトリガー
	signed char zr;
	//zlトリガー
	signed char zl;
	//矢印↑
	signed char up;
	//矢印↓
	signed char down;
	//矢印←
	signed char left;
	//矢印→
	signed char right;
	//右アナログスティックx
	signed char rx;
	//右アナログスティックy
	signed char ry;
	//左アナログスティックx
	signed char lx;
	//左アナログスティックy
	signed char ly;
	//the digital button click of LT and RT
	signed char blt;
	//the digital button click of LT and RT
	signed char brt;

	//右xスティック初期値
	static signed char rxSt;
	//右yスティック初期値
	static signed char rySt;
	//左xスティック初期値
	static signed char lxSt;
	//左yスティック初期値
	static signed char lySt;

	inline unsigned char formedRX(){
		signed char val = latest.rx - ((int) rxSt - 16) + 16;
		if (val < 0)val = 0;
		else if (31 < val)val = 31;
		return (unsigned char) val;
	}

	inline unsigned char formedRY(){
		signed char val = latest.ry - ((int) rySt - 16) + 16;
		if (val < 0)val = 0;
		else if (31 < val)val = 31;
		return (unsigned char) val;
	}

	inline unsigned char formedLX(){
		signed char val = latest.lx - ((int) lxSt - 32) + 32;
		if (val < 0)val = 0;
		else if (63 < val)val = 63;
		return (unsigned char) val;
	}

	inline unsigned char formedLY(){
		signed char val = latest.ly - ((int) lySt - 32) + 32;
		if (val < 0)val = 0;
		else if (63 < val)val = 63;
		return (unsigned char) val;
	}

	inline unsigned char rawRX(){
		return (unsigned char) latest.rx;
	}

	inline unsigned char rawRY(){
		return (unsigned char) latest.ry;
	}

	inline unsigned char rawLX(){
		return (unsigned char) latest.lx;
	}

	inline unsigned char rawLY(){
		return (unsigned char) latest.ly;
	}

private:

	//受信データ(生)
	unsigned char rawData[6];
	//データをフォーマットし、変数に代入
	void createData(void){
		lx = rawData[0] & 0x3F;
		ly = rawData[1] & 0x3F;
		rx = ((rawData[0] >> 3) & 0x18) | ((rawData[1] >> 5) & 0x06) | ((rawData[2] >> 7) & 0x01);
		ry = rawData[2] & 0x1F;
		l = ((rawData[2] >> 2) & 0x18) | ((rawData[3] >> 5) & 0x07);
		r = rawData[3] & 0x1F;

		right	= 0x01 & (rawData[4] >> 7);
		down	= 0x01 & (rawData[4] >> 6);
		blt		= 0x01 & (rawData[4] >> 5);
		select	= 0x01 & (rawData[4] >> 4);
		home	= 0x01 & (rawData[4] >> 3);
		start	= 0x01 & (rawData[4] >> 2);
		brt		= 0x01 & (rawData[4] >> 1);

		zl		= 0x01 & (rawData[5] >> 7);
		b		= 0x01 & (rawData[5] >> 6);
		y		= 0x01 & (rawData[5] >> 5);
		a		= 0x01 & (rawData[5] >> 4);
		x		= 0x01 & (rawData[5] >> 3);
		zr		= 0x01 & (rawData[5] >> 2);
		left	= 0x01 & (rawData[5] >> 1);
		up		= 0x01 & (rawData[5]     );
		
	}


	//クラシックコントローラの受信バイトをデコードする
	static inline unsigned char decodeByte(unsigned char data){
		return (data ^ 0x17) + 0x17;
	}

	//0を1バイト送る
	static unsigned char send0(void){
		Wire.beginTransmission(i2cAdd);	// transmit to device 0x52
		Wire.write(0x00);				// sends one byte
		Wire.endTransmission();			// stop transmitting
	}

};

