char* n_s(signed int value, char *chars, unsigned char& start);
//シーケンス制御用クラス
class Sequence
{
public:
	//ESC文字
	static const char esc = 0x1b;

	//XY座標を指定してカーソル移動
	static inline char* xy(unsigned char x, unsigned char y){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(y, st, i);
		st[i] = ';';
		i++;
		n_s(x, st, i);
		st[i] = 'H';
		st[i + 1] = '\0';
		return st;
	}

	//一行上に移動
	static inline char* up(void){
		st[0] = esc;
		st[1] = 'M';
		st[2] = '\0';
		return st;
	}
	//複数行上に移動
	static inline char* up(unsigned char value){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(value, st, i);
		st[i] = 'A';
		i++;
		st[i] = '\0';
		return st;
	}

	//複数行下に移動
	static inline char* down(unsigned char value){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(value, st, i);
		st[i] = 'B';
		i++;
		st[i] = '\0';
		return st;
	}

	//複数桁右に移動
	static inline char* right(unsigned char value){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(value, st, i);
		st[i] = 'C';
		i++;
		st[i] = '\0';
		return st;
	}

	//複数桁左に移動
	static inline char* left(unsigned char value){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(value, st, i);
		st[i] = 'D';
		i++;
		st[i] = '\0';
		return st;
	}

	//全画面消去
	static inline const char* clear(void){
		return clear_;
	}

	//カーソル位置からLength文字消去
	static inline char* clear(unsigned char length){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(length, st, i);
		st[i] = 'X';
		i++;
		st[i] = '\0';
		return st;
	}

	//単形消去 {(x1,y1),(x2,y2)}
	static char* clear(unsigned char X1, unsigned char Y1, unsigned char X2, unsigned char Y2){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(Y1, st, i);
		st[i] = ';';
		i++;
		n_s(X1, st, i);
		st[i] = ';';
		i++;
		n_s(Y2, st, i);
		st[i] = ';';
		i++;
		n_s(X2, st, i);
		st[i] = '$';
		i++;
		st[i] = 'z';
		return st;
	}


	//画面先頭からカーソル位置まで消去
	static inline char* clear2h(void){
		st[0] = esc;
		st[1] = '[';
		st[2] = '1';
		st[3] = 'J';
		st[4] = '\0';
		return st;
	}
	//カーソル位置から画面末尾まで消去
	static inline char* clear2e(void){
		st[0] = esc;
		st[1] = '[';
		st[2] = '0';
		st[3] = 'J';
		st[4] = '\0';
		return st;
	}

	//行頭からカーソル位置まで消去
	static inline char* clear2lh(void){
		st[0] = esc;
		st[1] = '[';
		st[2] = '1';
		st[3] = 'K';
		st[4] = '\0';
		return st;
	}
	//カーソル位置から行末まで消去
	static inline char* clear2le(void){
		st[0] = esc;
		st[1] = '[';
		st[2] = '0';
		st[3] = 'K';
		st[4] = '\0';
		return st;
	}

	//色などをデフォルトに戻す
	static inline char* font(void){
		st[0] = esc;
		st[1] = '[';
		st[2] = 'm';
		st[3] = '\0';
		return st;
	}

	//色などを設定する
	static inline char* font(unsigned char font_mode){
		i = 2;
		st[0] = esc;
		st[1] = '[';
		n_s(font_mode, st, i);
		st[i] = 'm';
		return st;
	}

	//範囲を指定して、色などを設定する
	static inline char* font(unsigned char font_mode
		, unsigned char X1, unsigned char Y1, unsigned char X2, unsigned char Y2){
		st[0] = esc;
		st[1] = '[';
		i = 2;
		n_s(Y1, st, i);
		st[i] = ';';
		i++;
		n_s(X1, st, i);
		st[i] = ';';
		i++;
		n_s(Y2, st, i);
		st[i] = ';';
		i++;
		n_s(X2, st, i);
		st[i] = ';';
		i++;
		n_s(font_mode, st, i);
		st[i] = '$';
		st[i + 1] = 'r';
		st[i + 2] = '\0';
		return st;
	}

	//色をRGB値から設定する
	static inline char* color(unsigned char fb//前景もしくは後景
		, unsigned char R, unsigned char G, unsigned char B){
		st[0] = esc;
		st[1] = '[';

		if (fb == 0)
		{
			st[2] = '3';
			st[3] = '8';
		}
		else if (fb == 10)
		{
			st[2] = '4';
			st[3] = '8';
		}
		else
		{
			return st;
		}
		st[4] = ';';
		st[5] = '2';
		st[6] = ';';
		i = 7;
		n_s(R, st, i);
		st[i] = ';';
		i++;
		n_s(G, st, i);
		st[i] = ';';
		i++;
		n_s(B, st, i);
		st[i] = 'm';
		i++;
		st[i] = '\0';
		return st;
	}

	//カーソルの種類を設定する
	static inline char* cursor(unsigned char cursor_mode){
		st[0] = esc;
		st[1] = '[';
		st[2] = cursor_mode + '0';
		st[3] = 'S';
		st[4] = 'P';
		st[5] = 'q';
		st[6] = '\0';
		return st;
	}

	//カーソルの表示状態を設定する
	static inline char* cursorv(unsigned char visibility){
		st[0] = esc;
		st[1] = '[';
		st[2] = '?';
		st[3] = '2';
		st[4] = '5';
		st[5] = visibility;
		st[6] = '\0';
		return st;
	}

	class window
	{
	public:
		//Windowのステータス変更
		static inline char* state(unsigned char states){
			st[0] = esc;
			st[1] = '[';
			st[2] = states;
			st[3] = 't';
			st[4] = '\0';
			return st;
		}

		//WindowのTitleのスタックを操作
		static inline char* title(unsigned char stack){
			st[0] = esc;
			st[1] = '[';
			st[2] = '2';
			st[3] = stack;
			st[4] = ';';
			st[5] = '0';
			st[6] = 't';
			st[7] = '\0';
			return st;
		}

		//WindowのTitleを変更する
		static inline char* title(const unsigned char* title){
			st[0] = esc;
			st[1] = ']';
			st[2] = '0';
			st[3] = ';';
			i = 4;
			while (st[i] = title[i - 4]);
			{
				i++;
			}
			st[i] = '\a';
			st[i + 1] = '0';
			return st;
		}

		//windowのサイズを変更する
		static inline char* size(unsigned char x, unsigned char y){
			st[0] = esc;
			st[1] = '[';
			st[2] = '8';
			st[3] = ';';
			i = 4;
			n_s(y, st, i);
			st[i] = ';';
			i++;
			n_s(x, st, i);
			st[i] = 't';
			st[i + 1] = '\0';
			return st;
		}
		//Windowのステータス変更用
		enum states
		{
			normal = '1',
			minimum = '2',
			front = '5',
			back = '6',
			re_draw = '7'
		};

		//タイトルのスタックの操作用
		enum title_stack{
			title_save = '2',
			title_back = '3'
		};
	private:

	};

	struct cursor_mode
	{
		//カーソル用
		unsigned char cursor;
		//カーソルの種類
		enum cursorEnum
		{
			block = 1,
			under_bar = 3,
			vertical = 5,
			brink = 0,
			no_brink = 1,
			visible = 'h',
			hidden = 'l'
		};
	};

	struct color_mode
	{
		//色用
		unsigned char color;
		//色用のEnum
		enum color_enum
		{
			front = 0,
			light = 60,
			black = 30,
			red,
			green,
			yellow,
			blue,
			magenta,
			cyan,
			white,
			def = 39,
			back = 10
		};
	};

	struct font_mode
	{
		//色用
		unsigned char font;
		//色用のEnum
		enum font_enum
		{
			def = 0,
			bold,
			underline = 4,
			blink,
			turn = 7
		};
	};

	union st_union
	{
		char st8[128];
		unsigned int st16[64];
		unsigned long st32[32];
		//inline char& operator[](unsigned char value){
		//	return st8[value];
		//}

		inline operator char*(){
			return st8;
		}
	};
private:
	static unsigned char i;
	static const char clear_ [];

	static st_union st;
	//static char st[128];
};
