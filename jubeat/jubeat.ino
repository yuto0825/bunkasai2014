//ボタンのピン番号[X][Y]
int bts [4][4];
//ボタンのキーボード
char key[4][4];

//キーボードが押されているか
bool pushed[4][4];

void setup()
{
	Keyboard.begin();

	//btsのセットアップ 2〜17(AN0,AN1)
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j <4; j++)
		{
			int val = i + j * 4 + 2;
			if (13 > val)
			{
				bts[i][j] = val;
			}
			else
			{
				bts[i][j] = val + 5;
			}
		}
	}

	key[0][0] = 'z';
	key[0][1] = 'a';
	key[0][2] = 'q';
	key[0][3] = '1';

	key[1][0] = 'x';
	key[1][1] = 's';
	key[1][2] = 'w';
	key[1][3] = '2';

	key[2][0] = 'c';
	key[2][1] = 'd';
	key[2][2] = 'e';
	key[2][3] = '3';

	key[3][0] = 'v';
	key[3][1] = 'f';
	key[3][2] = 'r';
	key[3][3] = '4';


	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			pinMode(bts[i][j], INPUT_PULLUP);
			pushed[i][j] = false;
		}
	}
}

void loop()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (digitalRead(bts[i][j]))
			{
				if (pushed[i][j])
				{
					Keyboard.release(key[i][j]);
					pushed[i][j] = false;
				}
			}
			else
			{
				if (!pushed[i][j])
				{
					Keyboard.press(key[i][j]);
					pushed[i][j] = true;
				}
			}
		}
	}
}
